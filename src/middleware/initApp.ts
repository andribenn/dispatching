import config from './config.ts';

const setQuantityElementsInListByPageHeight = () => {
  for (let i in config.quantityElementsInListByPageHeight) {
    if (window.innerHeight > Number(i)) {
      config.quantityElementsInList = config.quantityElementsInListByPageHeight[i];
    }
  }
}

export default () => {
  setQuantityElementsInListByPageHeight();
}