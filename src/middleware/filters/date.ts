const mappingMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

const getTwoDigitNumber = (number: number) => {
  let result: string = number.toString();
  if (number.toString().length < 2) {
    result = '0' + number.toString();
  }
  return result;
}

export function filteredDateInDayAndMonth (date: Date):string {
  return date.getUTCDate() + ' ' + mappingMonths[date.getUTCMonth() + 1];
}
export function filteredDateInFullDateWithoutTime (date: Date):string {
  return getTwoDigitNumber(date.getUTCDate()) + '.' + getTwoDigitNumber(date.getUTCMonth() + 1) + '.' + date.getUTCFullYear();
}

export function filteredDateInFullDate (date: Date) {
  return date.getUTCDate() + ' ' + mappingMonths[date.getUTCMonth() + 1] + ' ' + date.getUTCFullYear() + ', ' + getTwoDigitNumber(date.getUTCHours()) + ':' + getTwoDigitNumber(date.getUTCMinutes());
}

export function filteredDateInTime (date: Date) {
  return date.toTimeString().split(':').slice(0, 2).join(':')
  // return getTwoDigitNumber(date.getUTCHours()) + ':' + getTwoDigitNumber(date.getUTCMinutes());
}
