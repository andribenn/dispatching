import axios from 'axios';

export default {
  list() : Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}drivers/all`);
  },
  getDriver(driverId: number) : Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}drivers/get`, { id: driverId });
  },
  add(data: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}drivers/add`, data);
  },
  edit(data: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}drivers/update`, data);
  },
  getDriversByPartnerId(partnerId: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}drivers/all`, {partner_id: partnerId});
  },
}