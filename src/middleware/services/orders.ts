import axios from 'axios';
import {capitalizeFirstLetter} from '../filters/string.ts';

export default {
  list(): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}bookings/all`);
  },
  listByOrderType(orderType: string): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}bookings/all`, { status: orderType });
  },
  listByUserId(userId: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}bookings/all`, {user_id: userId});
  },
  getOrder(orderId: number): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}bookings/get`, { id: orderId });
  },
  getCities(city: string): Promise<any> {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}city/search?city=${capitalizeFirstLetter(city)}`);
  },
  getRoutes(from_city: string, to_city: string): Promise<any> {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}routes/search?fromCity=${capitalizeFirstLetter(from_city)}&toCity=${capitalizeFirstLetter(to_city)}`);
  },
  add(data: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}bookings/add`, data);
  },
  updateStatus(orderId: number, status: string): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}bookings/update-status`, { id: orderId, status: status });
  },
  getPartners(): Promise<any> {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}dispatching/partners`);
  },
  update(orderId: number, order: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}bookings/update-route-order`, { id: orderId, order: order });
  },
  updatePartner(orderId: number, formData: any): Promise<any> {
    const data = formData;
    data.id = orderId;
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}bookings/update-route-order`, data);
  },
}