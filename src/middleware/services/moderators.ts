import axios from 'axios';

export default {
  list(): Promise<any> {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}dispatching/moderators`);
  },
  add(data: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}dispatching/moderators/add`, data, {headers: {'Content-Type': 'multipart/form-data'}});
  },
  getModerator(moderatorId: any): Promise<any> {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}dispatching/moderators/${moderatorId}`);
  }
}