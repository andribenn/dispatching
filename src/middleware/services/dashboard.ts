import axios from 'axios';
export default {
  getStatistics () {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}dispatching/metric/info`);
  },
  getRevenue () {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}dispatching/metric/revenue`);
  }
}