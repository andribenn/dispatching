// import axios from 'axios';

export default {
  list(userId: number) : Promise<any> {
    // return axios.post(`${import.meta.env.VITE_API_DOMAIN}feedbacks/all`, { user_id: userId });

    // mockData
    const mockDataFeedbacks = {
      // тестовые feedbacks
      rating: 5,
      reviews_count: 653,
      reviews: [
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        },
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        },
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        },
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        },
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        },
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        },
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        },
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        },
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        },
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        },
        {
          title: 'Good hud',
          description: 'Very goooooooooood very gooood'
        }
      ],
    }

    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(mockDataFeedbacks);
      }, 300)
    });
  },
}