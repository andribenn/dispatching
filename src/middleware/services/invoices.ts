import axios from 'axios';

export default {
  list(userId: number): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}invoice/get`, { user_id: userId, calendar: false });
  },
  getInvoice(id: number): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}invoice/${id}/info`);
  }
}