import axios from 'axios';
export default {
  auth(token: string | null) : Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}user/auth`, { token });
  },
  login(email: string, password: string) : Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}user/login`, { email, password })
        .then((req) => {
          localStorage.setItem('auth_token', req.data.auth_token)
          return req
        })
  },
  checkEmail(email: string) : Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}user/forgot-password`, { email });
  },
  checkCode(email: string, code: string) : Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}user/check-code`, { email, code });
  },
  updatePassword(email: string, code: string) : Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}user/update-password`, { email, code });
  },
}
