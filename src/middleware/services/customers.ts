import axios from 'axios';

export default {
  list(): Promise<any> {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}customers`);
  },
  getCustomer(customerId: number): Promise<any> {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}customers/${customerId}`);
  },
  add(customer: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}customers/add`, customer);
  },
  edit(customerId: number, customer: any): Promise<any> {
    const data = customer;
    data['user_id'] = customerId;
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}customers/update`, data);
  }
}