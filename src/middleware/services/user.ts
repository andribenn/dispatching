import axios from 'axios';

export default {
  update(user: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}user/update`, user);
  }
}