import axios from 'axios';
import config from '../config.ts';
// import config from '../config.ts';

export default {
  list(page: number, roleId: any=null) : Promise<any> {
    let data = `?current_page=${page}&count_items=${config.quantityElementsInList}`;
    if (roleId !== null) {
      data = data + `&role_id=${roleId}`;
    }
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}dispatching/fleets` + data);
  },
  getFleet(fleetId: number): Promise<any> {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}dispatching/fleets/${fleetId}`);
  },
  add(data: any) {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}dispatching/fleets/add`, data, {headers: {'Content-Type': 'multipart/form-data'}});
  },
  update(fleetId: number, formData: any) {
    const data = formData;
    data.user_id = fleetId;
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}dispatching/fleets/update`, data, {headers: {'Content-Type': 'multipart/form-data'}});
  }
}