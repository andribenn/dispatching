import axios from 'axios';

export default {
  getContacts(): Promise<any> {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}dispatching/chat/contacts`);
  },
  getMessages(chatId: number, userId: number): Promise<any> {
    return axios.get(`${import.meta.env.VITE_API_DOMAIN}dispatching/chat/${chatId}`, {params: { user_id: userId }});
  },
  sendMsg(chatId: number, userId: number, msg: string): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}dispatching/chat/${chatId}/send-message`, {user_id: userId, message: msg});
  }
}