import axios from 'axios';

export default {
  countries(): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}profile/countries`);
  },
  citiesByCountryId(countryId: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}profile/cities`, {countryId: countryId});
  },
}