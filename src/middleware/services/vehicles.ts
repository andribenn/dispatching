import axios from 'axios';

export default {
  list(): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}vehicles/all`);
  },
  getVehicle(VehicleId: number) : Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}vehicles/get`, { id: VehicleId });
  },
  add(data: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}vehicles/add`, data);
  },
  getTypes(): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}vehicles/body-types`);
  },
  getBrands(): Promise<any> {
    return axios.get('https://cars-base.ru/api/cars');
  },
  getModelsByBrand(brandId: any): Promise<any> {
    return axios.get(`https://cars-base.ru/api/cars/${brandId}`);
  },
  update(data: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}vehicles/update`, data);
  },
  getVehiclesByPartnerId(partnerId: any): Promise<any> {
    return axios.post(`${import.meta.env.VITE_API_DOMAIN}vehicles/all`, {user_id: partnerId});
  },
}