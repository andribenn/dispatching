export default {
  menu: [
    {
      name: 'Dashboard',
      path: '/dashboard',
      iconOpened: new URL('../../src/static/icons/sidebar/menu/opened/dashboard.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/menu/closed/dashboard.svg', import.meta.url)
    },
    {
      name: 'Orders',
      path: '/orders/all',
      rootPath: '/orders',
      iconOpened: new URL('../../src/static/icons/sidebar/menu/opened/orders.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/menu/closed/orders.svg', import.meta.url)
    },
    {
      name: 'Customers',
      path: '/customers',
      iconOpened: new URL('../../src/static/icons/sidebar/menu/opened/customers.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/menu/closed/customers.svg', import.meta.url)
    },
    {
      name: 'Live map',
      path: '/live-map',
      iconOpened: new URL('../../src/static/icons/sidebar/menu/opened/live-map.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/menu/closed/live-map.svg', import.meta.url)
    },
    {
      name: 'Drivers',
      path: '/drivers',
      iconOpened: new URL('../../src/static/icons/sidebar/menu/opened/drivers.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/menu/closed/drivers.svg', import.meta.url)
    },
    {
      name: 'Fleets',
      path: '/fleets/all',
      rootPath: '/fleets',
      iconOpened: new URL('../../src/static/icons/sidebar/menu/opened/fleets.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/menu/closed/fleets.svg', import.meta.url)
    },
    {
      name: 'Vehicles',
      path: '/vehicles',
      iconOpened: new URL('../../src/static/icons/sidebar/menu/opened/vehicles.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/menu/closed/vehicles.svg', import.meta.url)
    },
    {
      name: 'Invoices',
      path: '/invoices',
      iconOpened: new URL('../../src/static/icons/sidebar/menu/opened/invoices.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/menu/closed/invoices.svg', import.meta.url)
    },
    {
      name: 'Feedback',
      path: '/feedback',
      iconOpened: new URL('../../src/static/icons/sidebar/menu/opened/feedback.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/menu/closed/feedback.svg', import.meta.url)
    },
    {
      name: 'Moderators',
      path: '/moderators',
      iconOpened: new URL('../../src/static/icons/sidebar/menu/opened/moderators.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/menu/closed/moderators.svg', import.meta.url)
    },
  ] as Menu,

  menuFooter: [
    {
      name: 'Chat',
      path: '/chat',
      action: (router) => router.push('/chat'),
      iconOpened: new URL('../../src/static/icons/sidebar/footer/opened/chat.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/footer/closed/chat.svg', import.meta.url)
    },
    {
      name: 'Settings',
      path: '/settings',
      action: (router) => router.push('/settings'),
      iconOpened: new URL('../../src/static/icons/sidebar/footer/opened/settings.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/footer/closed/settings.svg', import.meta.url)
    },
    {
      name: 'Log out',
      path: null,
      action: (router) => {
        localStorage.removeItem('token');
        router.push('/auth/login');
      },
      iconOpened: new URL('../../src/static/icons/sidebar/footer/opened/log-out.svg', import.meta.url),
      iconClosed: new URL('../../src/static/icons/sidebar/footer/closed/log-out.svg', import.meta.url)
    },
  ] as MenuFooter,

  graphs: {
    linear: {
      data: {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        datasets: [{
          label: '',
          data: [65, 59, 80, 81, 56, 55, 40],
          fill: false,
          borderColor: '#4370FF',
          tension: 0.4,
        },
          {
            label: '',
            data: [75, 29, 40, 101, 56, 35, 21],
            fill: false,
            borderColor: '#35C9FF',
            tension: 0.4,
          }],
      },
      options: {
        plugins: {
          legend: {
            display: false,
          }
        },
        maintainAspectRatio: true,
        aspectRatio: 3.741,
      }
    },
    doughnut: {
      data: {
        datasets: [{
          data: [] as any,
          backgroundColor: [] as any,
          hoverOffset: .1,
          borderColor: '#F7F9FB',
          borderJoinStyle: 'round',
          borderSkipped: 'left',
          borderWidth: 8,
          borderRadius: 18,
          borderAlign: 'inner',
          hoverBorderWidth: 2,
          hoverBorderRadius: 8,
        }],
      } as any
    }
  },

  user: {
    statuses: {
      new: {
        title: 'NEW',
        color: '#4370FF'
      },
      inactive: {
        title: 'Inactive',
        color: '#DA1E28'
      },
      approved: {
        title: 'Approved',
        color: '#008B28'
      },
      'pending time': {
        title: 'Pending',
        color: '#FFA52F'
      },
      checking: {
        title: 'Pending',
        color: '#FFA52F'
      },
      'need-verify': {
        title: 'Need verify',
        color: '#008B28'
      },
      declined: {
        title: 'Declined',
        color: '#DA1E28'
      },
      confirmed: {
        title: 'Approved',
        color: '#008B28'
      }
    } as UserStatuses,
    list: {
      titles: [
        {
          name: 'checkbox',
          type: 'icon'
        },
        {
          name: '#',
          type: 'text'
        },
        {
          name: 'Name',
          type: 'text'
        },
        {
          name: 'Email',
          type: 'text'
        },
        {
          name: 'Number',
          type: 'text'
        },
        {
          name: 'Country',
          type: 'text'
        },
        {
          name: 'Status',
          type: 'text'
        },
        {
          name: 'Created',
          type: 'text'
        },
        {
          name: 'Note',
          type: 'text'
        },
        {
          name: '',
          type: 'clear'
        },
      ] as List[],
      blockSizes: [3, 5, 13, 17, 14, 12, 13, 13, 6, 4] as number[]
    },
    roles: {
      1: 'Administrator',
      2: 'Dispatcher',
      3: 'Customer',
      4: 'Partner',
      5: 'Driver',
    } as any
  },

  orders: {
    navBarItemsList: [
      {
        name: 'All',
        pageName: 'all',
        status: false
      },
      {
        name: 'Pending',
        pageName: 'pending',
        status: false
      },
      {
        name: 'Market',
        pageName: 'market',
        status: false
      },
      {
        name: 'Planned',
        pageName: 'planned',
        status: false
      },
      {
        name: 'Canceled',
        pageName: 'fail',
        status: false
      },
      {
        name: 'Finished',
        pageName: 'complete',
        status: false
      }
    ] as NavBarItem[],
    list: {
      titles: [
        {
          name: 'checkbox',
          type: 'icon'
        },
        {
          name: 'Order #',
          type: 'text'
        },
        {
          name: 'Departure',
          type: 'text'
        },
        {
          name: 'Route',
          type: 'text'
        },
        {
          name: 'Passengers',
          type: 'text'
        },
        {
          name: 'Vehicles',
          type: 'text'
        },
        {
          name: 'Note',
          type: 'text'
        },
        {
          name: 'Price',
          type: 'text'
        },
        {
          name: 'Status',
          type: 'text'
        },
      ] as List[],
      blockSizes: [4, 8, 15, 22, 12, 9, 7, 7, 16] as number[]
    },
    types: {
      pending: {
        title: 'Pending',
        color: '#FFA52F'
      },
      planned: {
        title: 'Planed',
        color: '#D41CC1'
      },
      accepted: {
        title: 'Accepted',
        color: '#2C2C2C'
      },
      confirmed: {
        title: 'Confirmed',
        color: '#7514F3'
      },
      complete: {
        title: 'Finished',
        color: '#008B28'
      },
      market: {
        title: 'Market',
        color: '#0043CE'
      },
      fail: {
        title: 'Canceled',
        color: '#DA1E28'
      }
    } as OrderTypes
  },

  fleets: {
    navBarItemsList: [
      {
        name: 'All',
        pageName: 'all',
        status: false
      },
      {
        name: 'Become a driver',
        pageName: 'become-driver',
        status: false
      },
      {
        name: 'Become a partner',
        pageName: 'become-partner',
        status: false
      },
      {
        name: 'Become a Travel agent',
        pageName: 'become-travel-agent',
        status: false
      }
    ] as NavBarItem[],
    roleIdByPageName: {
      'become-driver': 4,
      'become-partner': 5,
      'become-travel-agent': 6
    } as any
  },

  vehicles: {
    types: {
      sedan: {
        title: 'SEDAN',
        color: '#0E855B'
      },
      van: {
        title: 'VAN',
        color: '#EC824A'
      },
      mpv: {
        title: 'MPV',
        color: '#1969A9'
      },
      'luxury sedan': {
        title: 'LUXURY',
        color: '#000000'
      },
      bus: {
        title: 'BUS',
        color: '#1969A9'
      }
    } as VehicleTypes,
    list: {
      titles: [
        {
          name: 'checkbox',
          type: 'icon'
        },
        {
          name: '#',
          type: 'text'
        },
        {
          name: 'Brand',
          type: 'text'
        },
        {
          name: 'Model',
          type: 'text'
        },
        {
          name: 'Company',
          type: 'text'
        },
        {
          name: 'Year',
          type: 'text'
        },
        {
          name: 'Class',
          type: 'text'
        },
        {
          name: 'Created',
          type: 'text'
        },
        {
          name: 'Status',
          type: 'text'
        },
        {
          name: '',
          type: 'clear'
        },
      ] as List[],
      blockSizes: [3, 5, 14, 14, 14, 10, 10, 14, 12, 4] as number[]
    },
    statuses: {
      new: {
        title: 'NEW',
        color: '#4370FF'
      },
      approved: {
        title: 'Approved',
        color: '#008B28'
      },
      'pending time': {
        title: 'Pending',
        color: '#FFA52F'
      },
      declined: {
        title: 'Decline',
        color: '#DA1E28'
      }
    } as VehicleStatuses,
  },

  customers: {
    list: {
      titles: [
        {
          name: 'checkbox',
          type: 'icon'
        },
        {
          name: '#',
          type: 'text'
        },
        {
          name: 'Name',
          type: 'text'
        },
        {
          name: 'Email',
          type: 'text'
        },
        {
          name: 'Number',
          type: 'text'
        },
        {
          name: 'Orders',
          type: 'text'
        },
        {
          name: 'Status',
          type: 'text'
        },
        {
          name: 'Created',
          type: 'text'
        },
        {
          name: 'Note',
          type: 'text'
        },
        {
          name: '',
          type: 'clear'
        },
      ] as List[],
      blockSizes: [3, 5, 13, 17, 14, 12, 13, 13, 6, 4] as number[]
    }
  },

  invoices: {
    statuses: {
      paid: {
        title: 'Paid',
        color: '#008B28'
      },
      pending: {
        title: 'Pending',
        color: '#FFA52F'
      },
      fail: {
        title: 'Fail',
        color: '#DA1E28'
      },
      complete: {
        title: 'Complete',
        color: '#008B28'
      }
    } as InvoiceStatuses,
    list: {
      titles: [
        {
          name: 'checkbox',
          type: 'icon'
        },
        {
          name: '#',
          type: 'text'
        },
        {
          name: 'Issued on',
          type: 'text'
        },
        {
          name: 'Period',
          type: 'text'
        },
        {
          name: 'Name',
          type: 'text'
        },
        {
          name: 'Total',
          type: 'text'
        },
        {
          name: 'Status',
          type: 'text'
        },
        {
          name: 'Action',
          type: 'text'
        },
        {
          name: '',
          type: 'clear'
        },
      ] as List[],
      blockSizes: [4, 5, 17, 25, 15, 10, 12, 4, 4] as number[]
    }
  },

  settings: {
    navBarItemsList: [
      {
        name: 'Profile',
        pageName: 'profile',
        status: false
      },
      {
        name: 'Change password',
        pageName: 'change-password',
        status: false
      },
    ] as NavBarItem[],
  },

  liveMap: {
    list: {
      titles: [
        {
          name: 'Order #',
          type: 'text'
        },
        {
          name: 'Departure',
          type: 'text'
        },
        {
          name: 'Route',
          type: 'text'
        },
        // {
        //   name: 'Note',
        //   type: 'text'
        // },
        {
          name: 'Status',
          type: 'text'
        },
        // {
        //   name: 'Header',
        //   type: 'text'
        // },
        // {
        //   name: 'Details',
        //   type: 'text'
        // },
      ] as List[],
      blockSizes: [10, 20, 30, 10, 10, 15, 5] as number[]
    }
  },

  quantityElementsInList: 8 as number,

  quantityElementsInListByPageHeight: {
    1200: 14,
    1100: 13,
    1000: 12,
    920: 10,
    900: 9,
    800: 8,
    740: 7,
    640: 6,
  } as QuantityElementsInListByPageHeight,

  mapboxAccessToken: 'pk.eyJ1IjoibXl0cmlwbGluZSIsImEiOiJjbHBoOGV6ZDAwNTJlMm1zNWo4YWFqODdjIn0.tfaHwuEiNjRiyd0Ge6MK6w' as string,
}
