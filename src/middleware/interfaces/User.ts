interface UserStatus {
  readonly title: string,
  readonly color: HEX
}

interface UserStatuses {
  readonly [key: string]: UserStatus
}