interface InvoiceStatus {
  readonly title: string,
  readonly color: HEX
}

interface InvoiceStatuses {
  readonly [key: string]: InvoiceStatus
}