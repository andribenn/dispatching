interface ErrorsUpdatePassword extends ErrorsAuth {
  passwordAgain: string[],
  invalidCode: boolean,
}