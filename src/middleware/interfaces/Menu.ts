interface MenuItem {
  name: string,
  path: string,
  rootPath?: string,
  iconOpened: any,
  iconClosed: any
}

interface MenuFooterItem {
  name: string,
  path: string | null,
  action: (router: any) => void,
  iconOpened: any,
  iconClosed: any
}

type Menu = MenuItem[];
type MenuFooter = MenuFooterItem[];