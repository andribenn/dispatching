interface NavBarItem {
  name: string,
  pageName: string,
  status: boolean
}