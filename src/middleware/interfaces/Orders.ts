interface OrderType {
  readonly title: string,
  readonly color: HEX
}

interface OrderTypes {
  readonly [key: string]: OrderType
}