interface List {
  readonly name: string,
  readonly width: number,
  readonly type: string
}
interface QuantityElementsInListByPageHeight {
  [key: number]: number
}