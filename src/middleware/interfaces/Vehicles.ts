interface VehicleType {
  readonly title: string,
  readonly color: HEX
}

interface VehicleTypes {
  readonly [key: string]: VehicleType
}

interface VehicleStatus {
  title: string,
  color: string
}
interface VehicleStatuses {
  readonly [key: string]: VehicleStatus
}