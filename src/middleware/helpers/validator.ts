export default {
  rules: {
    require: {
      maskValid: (val: string): boolean => val !== undefined && val.length !== 0,
      message: 'Please fill out this field.'
    },
    english: {
      maskValid: (val: string): boolean => (/^[A-Za-z0-9!"№;%:?*()_+@#$^&,. '/-]+$/).test(val),
      message: 'Only english'
    },
    numbers: {
      maskValid: (val: string): boolean => (/^[0-9+./ -]+$/).test(val),
      message: 'Only numbers'
    },
    email: {
      maskValid: (val: string): boolean => (/^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu).test(val),
      message: 'Only email'
    },
    password: {
      maskValid: (val: string): boolean => (/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/).test(val.toString()),
      message: 'Your password must be at least 6 characters long, including one uppercase letter and one number.'
    }
  } as any,
  validation(dataInput: string, regulations: string[]) {
    let errors: string[] = [];

    for (let i in regulations) {
      if (!this.rules[regulations[i]].maskValid(dataInput)) {
        errors.push(this.rules[regulations[i]].message)
      }
    }

    return errors;
  },
  checkErrors(errors: any) {
    let status = false;
    for (let error in errors as any) {
      if (errors[error].constructor === Array) {
        if (errors[error].length > 0) {
          status = true;
          break;
        }
      } else if (typeof errors[error] === 'object') {
        let objectName: string = error;
        for (let error in errors as any) {
          if (errors[objectName][error].constructor === Array && errors[objectName][error][0]?.length) {
            status = true;
            break;
          }
        }
      }
    }

    return status
  },
  updateErrors(errors: any) {
    for (let error in errors as any) {
      if (errors[error].constructor === Array && errors[error][0]) {
        errors[error] = [];
      } else if (typeof errors[error] === 'object') {
        let objectName: string = error;
        for (let error in errors as any) {
          if (typeof errors[objectName][error] === 'string' && errors[objectName][error].length) {
            errors[objectName][error][0] = [];
          }
        }
      }
    }
  }
}