import axios from 'axios'

axios.defaults.withCredentials = true;

axios.interceptors.request.use((config) => {
    config.headers.Authorization = `Bearer ${localStorage.getItem('auth_token')}`
    return config
})

axios.interceptors.response.use(
    (response) => response,
    (error) => {
        if (error.response.status === 401) {
            localStorage.removeItem('auth_token');
            localStorage.removeItem('token');
            window.location.href = '/auth/login';
        }
        return Promise.reject(error);
    }
);

const api = axios.create({ baseURL: import.meta.env.VITE_API_DOMAIN })

api.interceptors.request.use((config) => {
    config.headers.Authorization = `Bearer ${localStorage.getItem('auth_token')}`
    return config
})

api.interceptors.response.use(
    (response) => response,
    (error) => {
        if (error.response.status === 401) {
            localStorage.removeItem('auth_token');
            localStorage.removeItem('token');
            window.location.href = '/auth/login';
        }
        return Promise.reject(error);
    }
);

export { api }
