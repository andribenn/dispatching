import { createApp } from 'vue'
import './static/scss/index.scss'
import App from './App.vue'
import Router from './router/index.ts'
import { createPinia } from 'pinia';
import { PointElement, LineElement, ArcElement, BarElement, CategoryScale, Chart as ChartJS, Legend, LinearScale, Title, Tooltip} from 'chart.js';


import VueMapboxTs from "vue-mapbox-ts";
ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend, ArcElement, LineElement, PointElement);

import VueDatePicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'

import VueTelInput from 'vue3-tel-input'
import 'vue3-tel-input/dist/vue3-tel-input.css'
import './middleware/helpers/axios';

createApp(App)
  .use(createPinia())
  .use(Router)
  .use(VueMapboxTs)
  .use(VueTelInput)
  .component('v-date-picker', VueDatePicker)
  .mount('#app')
