import { defineStore } from 'pinia';

export const useAppStore = defineStore('app', {
  state: () => ({
    isLoad: false,
    isAuthorizationCompleted: false,
  }),
  actions: {
    setLoadStatus(status: boolean) {
      this.isLoad = status;
    },
    hasAuthorizationCompleted() {
      this.isAuthorizationCompleted = true;
    }
  }
})