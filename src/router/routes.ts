const routes = [
  // auth
  {
    name: 'auth',
    path: '/auth',
    component: () => import('../layouts/auth.vue'),
    children: [
      {
        name: 'login',
        path: '/auth/login',
        component: () => import('../pages/auth/login.vue')
      },
      {
        name: 'update-password',
        path: '/auth/update-password',
        component: () => import('../pages/auth/update-password.vue')
      },
    ]
  },
  // root
  {
    name: 'root',
    path: '/',
    component: () => import('../layouts/default.vue'),
    meta: { isAuth: true },
    children: [
      {
        name: 'dashboard',
        path: '/dashboard',
        component: () => import('../pages/dashboard.vue'),
        meta: {
          title: 'Good morning!',
          subtitle: 'Welcome back to dashboard.',
          themeHeader: 'light'
        }
      },
      {
        name: 'orders',
        path: '/orders/:orderType',
        component: () => import('../pages/orders/index.vue'),
      },
      {
        name: 'order',
        path: '/orders/view/:id',
        component: () => import('../pages/orders/view.vue'),
        meta: {
          themePage: 'dark'
        }
      },
      {
        name: 'orderNew',
        path: '/orders/new',
        component: () => import('../pages/orders/new.vue'),
        meta: {
          themePage: 'dark'
        }
      },
      {
        name: 'customers',
        path: '/customers',
        component: () => import('../pages/customers/index.vue'),
        meta: {
          title: 'Customers list'
        }
      },
      {
        name: 'customer',
        path: '/customers/view/:id',
        component: () => import('../pages/customers/view.vue'),
        meta: {
          title: 'Customer details',
          themePage: 'dark',
          paginationTheme: 'white',
        }
      },
      {
        name: 'live-map',
        path: '/live-map',
        component: () => import('../pages/live-map.vue'),
        meta: {
          themeHeader: 'clear',
          showFooter: false
        }
      },
      {
        name: 'drivers',
        path: '/drivers',
        component: () => import('../pages/drivers/index.vue'),
        meta: {
          title: 'Drivers List',
        }
      },
      {
        name: 'driversNew',
        path: '/drivers/new',
        component: () => import('../pages/drivers/new.vue'),
        meta: {
          themePage: 'dark',
          title: 'New Driver',
        }
      },
      {
        name: 'driver',
        path: '/drivers/view/:id',
        component: () => import('../pages/drivers/view.vue'),
        meta: {
          title: 'Driver details',
        }
      },
      {
        name: 'fleets',
        path: '/fleets/:fleetType',
        component: () => import('../pages/fleets/index.vue'),
        meta: {
          title: 'Fleets List',
        }
      },
      {
        name: 'fleetsNew',
        path: '/fleets/new',
        component: () => import('../pages/fleets/new.vue'),
        meta: {
          themePage: 'dark',
          title: 'New Partner',
        }
      },
      {
        name: 'fleet',
        path: '/fleets/view/:id',
        component: () => import('../pages/fleets/view.vue'),
        meta: {
          themePage: 'dark',
          title: 'Fleet',
        }
      },
      {
        name: 'vehicles',
        path: '/vehicles',
        component: () => import('../pages/vehicles/index.vue'),
        meta: {
          title: 'Vehicles List',
        }
      },
      {
        name: 'vehiclesNew',
        path: '/vehicles/new',
        component: () => import('../pages/vehicles/new.vue'),
        meta: {
          themePage: 'dark',
          title: 'New Vehicle',
        }
      },
      {
        name: 'vehicle',
        path: '/vehicles/view/:id',
        component: () => import('../pages/vehicles/view.vue'),
        meta: {
          title: 'Vehicle details',
        }
      },
      {
        name: 'invoices',
        path: '/invoices',
        component: () => import('../pages/invoices/index.vue'),
        meta: {
          themePage: 'grey',
          title: 'Invoices List',
        }
      },
      {
        name: 'invoice',
        path: '/invoices/view/:id',
        component: () => import('../pages/invoices/view.vue'),
        meta: {
          themePage: 'dark',
          title: 'Invoice',
        }
      },
      {
        name: 'feedback',
        path: '/feedback',
        component: () => import('../pages/feedback.vue'),
        meta: {
          themePage: 'dark',
          title: 'Feedback List',
        }
      },
      {
        name: 'moderators',
        path: '/moderators',
        component: () => import('../pages/moderators/index.vue'),
        meta: {
          themePage: 'dark'
        }
      },
      {
        name: 'moderator',
        path: '/moderators/view/:id',
        component: () => import('../pages/moderators/view.vue'),
        meta: {
          themePage: 'dark'
        }
      },
      {
        name: 'chat',
        path: '/chat',
        component: () => import('../pages/chat.vue'),
        meta: {
          themePage: 'dark'
        }
      },
      {
        name: 'Settings',
        path: '/Settings',
        component: () => import('../pages/settings.vue'),
      },
    ]
  },
];

export default routes;
